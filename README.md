# README #

Posterior sample data and Bilby ini files accompanying the paper [***Improved analysis of GW190412 with a precessing numerical relativity surrogate waveform model***](https://arxiv.org/abs/2010.04848)

These files can be used to reproduce some of the main results, namely table 1 and associated figures. Posteriors for GW190412 are recovered with 
a numerical relativity surrogate model, NRSur7dq4, and three different phenomenological models, PhenomPv3HM, PhenomXPHM, and PhenomXPHM.

Some of the ini files might need to be updated for your particular computing resources (e.g. SLURM vs PBS).

### Runtime environment ###

Please note you must have already installed [Bilby](https://lscsoft.docs.ligo.org/bilby/installation.html), 
[parallel Bilby](https://lscsoft.docs.ligo.org/parallel_bilby/installation), and [gwsurrogate](https://github.com/sxs-collaboration/gwsurrogate). 

To reproduce the environment similar to the one used to compile resultes reported in the paper, an example conda environment file is provided in ``settings/gw190412_pe.yml``. You can import this env by doing

```bash
>>> conda env create -f gw190412_pe.yml
```

lalsuite version information

```
11:54 bilby INFO    : Using lal version 7.1.2
11:54 bilby INFO    : Using lal git version Branch: None;Tag: lalsuite-v6.82;Id: cf792129c2473f42ce6c6ee21d8234254cefd337;
11:54 bilby INFO    : Using lalsimulation version 2.5.1
11:54 bilby INFO    : Using lalsimulation git version Branch: None;Tag: lalsuite-v6.82;Id: cf792129c2473f42ce6c6ee21d8234254cefd337;
```


### Quick start ###

For example, to reproduce the PhenomXPHM PE run, do

```bash
>>> git clone git@bitbucket.org:tausigmaislam/gw190412_pe.git
>>> cd settings
>>> ln -s ../psds/ psds
>>> ln -s ../strain/ strain
>>> ln -s ../calibration/ calibration
>>> parallel_bilby_generation GW190412_IMRPhenomXPHM_40Hz.ini
>>> cat outdir_IMRPhenomX_GW190412_40Hz/submit/analysis_IMRPhenomX_GW190412_40Hz_0.sh  # check slurm settings for your machine
```

For example, to reproduce the NRSur7dq4 PE run, do... *coming soon*! This run requires a git patch so that Bilby can use gwsurrogate models. 


### Contact ###

* Tousif Islam ( tislam - at - umassd.edu )
* Scott Field
* Carl-Johan Haster
* Rory Smith

### Cite ###

If you use these data files, please cite 

```
@article{islam2020improved,
  title={Improved analysis of GW190412 with a precessing numerical relativity surrogate waveform model},
  author={Islam, Tousif and Field, Scott E and Haster, Carl-Johan and Smith, Rory},
  journal={arXiv preprint arXiv:2010.04848},
  year={2020}
}
```
