import numpy as np
import matplotlib.pyplot as plt

f1, psd1 = np.loadtxt('GW190412_H1_LVC_PSD.txt', unpack=True)
f2, psd2 = np.loadtxt('GW190412_L1_LVC_PSD.txt', unpack=True)
f3, psd3 = np.loadtxt('GW190412_V1_LVC_PSD.txt', unpack=True)

plt.figure(figsize=(13,6))
plt.subplot(121)
plt.title('New')

plt.loglog(f1, psd1**0.5, label='H1')
plt.loglog(f2, psd2**0.5, label='L1')
plt.loglog(f3, psd3**0.5, label='V1')
plt.legend()
plt.xlim(15,2048)
plt.ylim(1e-24,1e-20)
#plt.savefig('psd_new.png')
#plt.close()

f1, psd1 = np.loadtxt('GW190412_PSD_4s_srate2kHz_H1.dat', unpack=True)
f2, psd2 = np.loadtxt('GW190412_PSD_4s_srate2kHz_L1.dat', unpack=True)
f3, psd3 = np.loadtxt('GW190412_PSD_4s_srate2kHz_V1.dat', unpack=True)

#plt.figure(figsize=(6,6))
plt.subplot(122)
plt.title('Old')
plt.loglog(f1, psd1**0.5, label='H1')
plt.loglog(f2, psd2**0.5, label='L1')
plt.loglog(f3, psd3**0.5, label='V1')
plt.legend()
plt.xlim(15,2048)
plt.ylim(1e-24,1e-20)
plt.savefig('psd.png')
plt.close()


